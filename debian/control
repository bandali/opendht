Source: opendht
Section: libs
Priority: optional
Maintainer: Amin Bandali <bandali@gnu.org>
Build-Depends: debhelper-compat (= 12),
               cmake,
               dh-exec,
               pkg-config,
               libcppunit-dev,
               libgnutls28-dev,
               libmsgpack-cxx-dev | libmsgpack-dev (>= 1.2),
               libmsgpack-cxx-dev | libmsgpack-dev (<= 4.0.0),
               libreadline6-dev,
               libncurses-dev,
               libargon2-dev,
               librestinio-dev,
               libasio-dev,
               libjsoncpp-dev,
               libhttp-parser-dev,
               libssl-dev,
               libfmt-dev,
               nettle-dev,
# Python 3 bindings
               dh-python,
               python3-all-dev:any,
               python3-setuptools,
               cython3
Standards-Version: 4.6.2
Homepage: https://github.com/savoirfairelinux/opendht
Vcs-Git: https://salsa.debian.org/debian/opendht.git
Vcs-Browser: https://salsa.debian.org/debian/opendht
Rules-Requires-Root: no

Package: libopendht-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends},
         libopendht2 (=${binary:Version}),
         libgnutls28-dev,
         libmsgpack-cxx-dev | libmsgpack-dev (>= 1.2),
         libmsgpack-cxx-dev | libmsgpack-dev (<= 4.0.0),
         libreadline6-dev,
         libncurses-dev,
         libargon2-dev,
         librestinio-dev,
         libasio-dev,
         libjsoncpp-dev,
         libhttp-parser-dev,
         libssl-dev,
         libfmt-dev,
         nettle-dev
Description: Development files for the libopendht library
 OpenDHT is a lightweight C++17 Distributed Hash Table implementation.
 .
 OpenDHT provides an easy to use distributed in-memory data store.
 Every node in the network can read and write values to the store.
 Values are distributed over the network, with redundancy.
 .
 Overview of features:
  * Lightweight and scalable, designed for large networks and small
    devices
  * High resilience to network disruption
  * Public key cryptography layer providing optional data signature
    and encryption (using GnuTLS)
  * IPv4 and IPv6 support
  * Clean and powerful C++17 map API
  * Bindings for C and Python 3
  * REST API with optional HTTP client+server with push notification
    support
 .
 This package contains the C++ library headers and other
 development files.

Package: libopendht2
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Lightweight C++17 distributed hash table implementation
 OpenDHT is a lightweight C++17 Distributed Hash Table implementation.
 .
 OpenDHT provides an easy to use distributed in-memory data store.
 Every node in the network can read and write values to the store.
 Values are distributed over the network, with redundancy.
 .
 Overview of features:
  * Lightweight and scalable, designed for large networks and small
    devices
  * High resilience to network disruption
  * Public key cryptography layer providing optional data signature
    and encryption (using GnuTLS)
  * IPv4 and IPv6 support
  * Clean and powerful C++17 map API
  * Bindings for C and Python 3
  * REST API with optional HTTP client+server with push notification
    support
 .
 This package contains the C++ shared library.

Package: libopendht-c-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends},
         libopendht-c2 (=${binary:Version}),
         libopendht-dev,
         libgnutls28-dev,
         libargon2-dev
Description: Development files for the libopendht-c library
 OpenDHT is a lightweight C++17 Distributed Hash Table implementation.
 .
 OpenDHT provides an easy to use distributed in-memory data store.
 Every node in the network can read and write values to the store.
 Values are distributed over the network, with redundancy.
 .
 Overview of features:
  * Lightweight and scalable, designed for large networks and small
    devices
  * High resilience to network disruption
  * Public key cryptography layer providing optional data signature
    and encryption (using GnuTLS)
  * IPv4 and IPv6 support
  * Clean and powerful C++17 map API
  * Bindings for C and Python 3
  * REST API with optional HTTP client+server with push notification
    support
 .
 This package contains the C bindings library header and other
 development files.

Package: libopendht-c2
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libopendht2 (=${binary:Version})
Description: Lightweight C++17 distributed hash table implementation - C wrapper
 OpenDHT is a lightweight C++17 Distributed Hash Table implementation.
 .
 OpenDHT provides an easy to use distributed in-memory data store.
 Every node in the network can read and write values to the store.
 Values are distributed over the network, with redundancy.
 .
 Overview of features:
  * Lightweight and scalable, designed for large networks and small
    devices
  * High resilience to network disruption
  * Public key cryptography layer providing optional data signature
    and encryption (using GnuTLS)
  * IPv4 and IPv6 support
  * Clean and powerful C++17 map API
  * Bindings for C and Python 3
  * REST API with optional HTTP client+server with push notification
    support
 .
 This package contains the C shared library.

Package: python3-opendht
Architecture: any
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
         libopendht2 (=${binary:Version})
Description: Python 3 bindings for libopendht C++ library
 OpenDHT is a lightweight C++17 Distributed Hash Table implementation.
 .
 OpenDHT provides an easy to use distributed in-memory data store.
 Every node in the network can read and write values to the store.
 Values are distributed over the network, with redundancy.
 .
 Overview of features:
  * Lightweight and scalable, designed for large networks and small
    devices
  * High resilience to network disruption
  * Public key cryptography layer providing optional data signature
    and encryption (using GnuTLS)
  * IPv4 and IPv6 support
  * Clean and powerful C++17 map API
  * Bindings for C and Python 3
  * REST API with optional HTTP client+server with push notification
    support
 .
 This package contains the Python 3 bindings.

Package: dhtnode
Architecture: any
Section: net
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libopendht2 (=${binary:Version}),
         adduser
Description: OpenDHT node binary
 OpenDHT is a lightweight C++17 Distributed Hash Table implementation.
 .
 OpenDHT provides an easy to use distributed in-memory data store.
 Every node in the network can read and write values to the store.
 Values are distributed over the network, with redundancy.
 .
 Overview of features:
  * Lightweight and scalable, designed for large networks and small
    devices
  * High resilience to network disruption
  * Public key cryptography layer providing optional data signature
    and encryption (using GnuTLS)
  * IPv4 and IPv6 support
  * Clean and powerful C++17 map API
  * Bindings for C and Python 3
  * REST API with optional HTTP client+server with push notification
    support
 .
 This package contains the dhtnode binary.
